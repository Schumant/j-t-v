<This is an old lab for _JRockit_ Mission Control. There is currently no corresponding lab for Java Mission Control,
although you can use JFR and/or JOverflow to solve the memory leak.>
 
This application has a memory leak. 

1. Run Leak.
2. Connect with Memleak.
3. Let it run for a while.
4. What types (instances of what types) do you think are leaking?
5. Can you fix the leak?
6. Run MultiLoaderLeak to try hunting for leaks in different class loaders.









































Check chapter 10 for hints.