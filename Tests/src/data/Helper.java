package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Helper {
	
	public static void schreibeDaten() throws IOException{
		String inhalt = "";
		FileWriter fw = new FileWriter("txtFiles/test.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		for(int i = 0; i < 400000; i++){
			//System.out.println("Wert" + i);
			inhalt += ("\nWert: "+ i);
			//initList.add(i);
		}
		bw.write(inhalt);
		bw.close();
	}
	
	public static void leseDaten() throws IOException{
		//String temp = "";
		FileReader fr = new FileReader("txtFiles/test.txt");
	    BufferedReader br = new BufferedReader(fr);
	    String zeile = br.readLine();

	    while( zeile != null )
	    {
	      System.out.println(zeile);
	      //temp = temp + "\n" + zeile;
	      zeile = br.readLine();
	    }

	    br.close();
		//return zeile;
	}

}
